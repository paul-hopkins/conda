.. LSCSoft Conda documentation master file, created by
   sphinx-quickstart on Wed Feb  6 08:39:38 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. sectionauthor:: Duncan Macleod <duncann.macleod@ligo.org>
.. title:: LSCSoft Conda
.. _lscsoft-conda:

.. include:: ../README.rst

.. toctree::
   :caption: Documentation
   :maxdepth: 2

   usage/index
   environments/index
   compiling/index
   packaging/index
   tips/index
