.. _lscsoft-conda-reference-environments:

======================
Reference environments
======================

The LSCSoft Conda team currently defines reference environments for a number
of versions of Python.
You can install these by downloading the relevant YAML file from the repository
and installing them.


.. _lscsoft-conda-pre-built-environments:

----------------------
Pre-built environments
----------------------

The reference environments are pre-built and distributed using
`CVMFS <https://cvmfs.readthedocs.io/>`_.
You can configure your system to use the distributed conda installation:

.. code-block:: bash

   source /cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/etc/profile.d/conda.sh

You can then list the available environments:

.. code-block:: bash

   conda env list

which will print something like:

.. program-output:: sh environments/list.sh

You can then activate one of these environments:

.. code-block:: bash

   conda activate ligo-py37


.. _lscsoft-conda-available-environments:

----------------------
Available environments
----------------------

.. toctree::
   :glob:

   *
