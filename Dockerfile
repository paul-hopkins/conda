FROM debian:stretch

ARG INSTALL_PATH=/cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH $INSTALL_PATH/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install --yes \
      wget \
      bzip2 \
      ca-certificates \
      libglib2.0-0 \
      libxext6 \
      libsm6 \
      libxrender1 \
      git \
      mercurial \
      subversion && \
    apt-get clean

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /root/anaconda.sh && \
    mkdir -p $(dirname $INSTALL_PATH) && \
    /bin/bash /root/anaconda.sh -b -p $INSTALL_PATH && \
    rm -f ~/anaconda.sh && \
    . $INSTALL_PATH/etc/profile.d/conda.sh && \
    conda install --yes \
      conda \
      conda-build \
      conda-verify && \
    conda config --system --prepend channels conda-forge

COPY environment-py27.yml /tmp
RUN conda env create -f /tmp/environment-py27.yml

COPY environment-py36.yml /tmp
RUN conda env create -f /tmp/environment-py36.yml

COPY environment-py37.yml /tmp
RUN conda env create -f /tmp/environment-py37.yml

RUN conda clean --all --yes

FROM scratch
ARG INSTALL_PATH=/cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/
COPY --from=0 $INSTALL_PATH /
